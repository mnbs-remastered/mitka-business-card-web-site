let express = require("express"),
    path = require('path'),
    nodemailer = require('nodemailer'),
    bodyParser = require('body-parser');

const MongoClient = require('mongodb').MongoClient;
const jsonParser = express.json();
const ObjectID = require('mongodb').ObjectID;

let app = express();

app.use((req, res, next) => {
    res.header("Access-Control-Allow-Origin", "*");
    res.header("Access-Control-Allow-Headers", "Origin, X-reqed-With, Content-Type, Accept, Authorization");
    res.header("Access-Control-Allow-Methods", "GET, POST, PUT, DELETE, OPTIONS");
    next();
});

app.use(express.static('src'));

app.use(bodyParser.urlencoded({extended: true}));
app.use(bodyParser.json());

app.use(express.static(__dirname + "/public"));

const mongoClient = new MongoClient("mongodb://localhost:27017/", { useNewUrlParser: true });

mongoClient.connect(function(err, client){
    if(err) return console.log(err);
    let dbClient = client;
    app.locals.collection1 = client.db("mitka").collection("single_mitka");
    app.locals.collection2 = client.db("mitka").collection("set_mitka");
    app.listen(3030, function(){
        console.log("Server1 started...");
    });
});

app.post('/individ_order', (req, res) => {
    let transporter = nodemailer.createTransport({
        service: 'gmail',
        auth: {
            user: 'maryandboys2018@gmail.com',
            pass: 'mitkalpml'
        }
    });

    let mailOptions = {
        from: 'maryandboys2018@gmail.com',
        to: 'maryandboys2018@gmail.com',
        subject: 'Індивідуальне замовлення мітки',
        text: 'Привіт! Я, ' + req.body.name + ' ' + req.body.surname + ', ' + 'хочу замовити індивідуальну мітку.\n' +
            'Текст: ' + req.body.text + '\n' +
            'Колір: ' + req.body.color + '\n' +
            'Форма: ' + req.body.shape + '\n' +
            'Кількість: ' + req.body.quantity + '\n' +
            'Ви можете зв\'язатися зі мною за таким email: ' + req.body.email
    };

    transporter.sendMail(mailOptions, (err, data) => {
        if (err) {
            return log('Error occurs', err);
        }
        return log('Email sent!!!');
    });
});

app.post('/set_order', (req, res) => {
    let transporter = nodemailer.createTransport({
        service: 'gmail',
        auth: {
            user: 'maryandboys2018@gmail.com',
            pass: 'mitkalpml'
        }
    });
    let mailOptions = {
        from: 'maryandboys2018@gmail.com',
        to: 'maryandboys2018@gmail.com',
        subject: 'Замовлення мітки',
        text: 'Привіт! Я, ' + req.body.name + ', ' + 'хочу замовити мітку.\n' +
            'Найменування товару: ' + req.body.mitkaName + '\n' +
            'Колір: ' + req.body.mitkaColor + '\n' +
            'Кількість: ' + req.body.mitkaNumber + '\n' +
            'Ціна: ' + req.body.mitkaPrice + '\n' +
            'Номер телефону: ' + req.body.phone+ '\n' +
            'Місто: ' + req.body.city + '\n' +
            'Номер відділення НП: ' + req.body.office + '\n' +
            'E-mail: ' + req.body.email
    };

    transporter.sendMail(mailOptions, (err, data) => {
        if (err) {
            return log('Error occurs', err);
        }
        return log('Email sent!!!');
    });
});

let server = app.listen(3000, function(){
    let port = server.address().port;
    console.log("Server started at http://localhost:%s", port);
});

app.post("/singlemitka", jsonParser, function (req, res) {

    if(!req.body) return res.sendStatus(400);

    const mitkaNumber = req.body.numberOfMitka;
    const mitkaColor = req.body.color;
    const mitka = {number: mitkaNumber, color: mitkaColor};

    const collection = req.app.locals.collection1;
    collection.insertOne(mitka, function(err, result){

        if(err) return console.log(err);
        res.send(mitka);
    });
});

app.post("/setmitka", jsonParser, function (req, res) {

    if(!req.body) return res.sendStatus(400);

    const mitkaFirstColor = req.body.firstColor;
    const mitkaSecondColor = req.body.secondColor;
    const mitkaThirdColor = req.body.thirdColor;
    const mitkaFourthColor = req.body.fourthColor;
    const mitkaColors = {firstColor: mitkaFirstColor, secondColor: mitkaSecondColor,
        thirdColor: mitkaThirdColor, fourthColor: mitkaFourthColor};

    const collection = req.app.locals.collection2;
    collection.insertOne(mitkaColors, function(err, result){

        if(err) return console.log(err);
        res.send(mitkaColors);
    });
});

app.get("/singlemitka", function(req, res){

    const collection = req.app.locals.collection1;
    collection.find({}).toArray(function(err, mitka){

        if(err) return console.log(err);
        console.log(mitka);
        res.send(mitka)
    });
});

app.get("/setMitka", function(req, res){

    const collection = req.app.locals.collection2;
    collection.find({}).toArray(function(err, setMitka){

        if(err) return console.log(err);
        res.send(setMitka)
    });
});

app.delete("/singlemitka/:id", function(req, res){
    let id = req.params.id;
    const collection = req.app.locals.collection1;
    collection.deleteOne({ _id: ObjectID.ObjectID(id)}, function (err, results) {
    });

    res.send({ success: id });
});

app.delete("/setmitka/:id", function(req, res){
    let id = req.params.id;
    const collection = req.app.locals.collection2;
    collection.deleteOne({ _id: ObjectID.ObjectID(id)}, function (err, results) {
    });

    res.send({ success: id });
});

app.delete("/singlemitkaAll", function(req, res){
    const collection = req.app.locals.collection1;
    collection.deleteMany({}, function (err, results) {
    });

    res.send({ success: null});
});

app.delete("/setmitkaAll", function(req, res){
    const collection = req.app.locals.collection2;
    collection.deleteMany({}, function (err, results) {
    });

    res.send({ success: null});
});