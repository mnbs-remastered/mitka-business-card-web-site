const DOMAIN = 'http://localhost:3000/';
const DOMAINDB = 'http://localhost:3030/';

let overall_sum = 0;
let price = document.getElementById("price");
let total_price = document.getElementById("total_price");
let id_arr = [];
let id_arr_set = [];
let i = 0;
let j = 0;
let counter_order = 0;
let mitkName = [];
let mitkaColor = [];
let mitkaNumber = [];
let mitkaPrice = [];

let modal = document.getElementById("myModal");

let btn = document.getElementById("set_order");

let span = document.getElementsByClassName("close")[0];

let sendButton = document.getElementById("send_button");

function pageLoad() {
    getFromServer('singlemitka',
        singleMitka => singleMitka.map(addSingleElement));
    getFromServer('setmitka',
        setMitka => setMitka.map(addSingleElementSet));
}

function getFromServer(key, callback) {
    let req = new XMLHttpRequest();
    req.responseType = 'json';
    req.open('GET', DOMAINDB + key, true);
    req.onload = () => req.status === 200 ? callback(req.response) : console.log(req.response);
    req.send(null);
}

function addSingleElement(singleMitka) {
    let new_element = document.getElementById("single_mitka_item");
    let new_order_element = document.getElementById("mitka_order_item");
    let number = singleMitka.number;
    let color = singleMitka.color;
    let id = singleMitka._id;
    id_arr.push(id);
    overall_sum += number * 10;
    let colorElement;
    switch (color) {
        case "orange_white":
            colorElement = "оранжево-білий";
            break;
        case "purple_white":
            colorElement = "фіолетово-білий";
            break;
        case "white_gray":
            colorElement = "біло-сірий";
            break;
        case "yellow_gray":
            colorElement = "жовто-сірий";
            break;
        case "blue_yellow":
            colorElement = "блакитно-жовтий";
            break;
        case "red_white":
            colorElement = "червоно-білий";
            break;
    }
    mitkName.push("Mitka-standard");
    mitkaColor.push(colorElement);
    mitkaNumber.push(singleMitka.number);
    mitkaPrice.push("$" + singleMitka.number * 10);
    let element = document.createElement("div");
    let order_element = document.createElement("div");
    order_element.setAttribute("class", "row");
    order_element.innerHTML = "<div class=\"col-md-2\">\n" +
        "                                    <p id=\"number\">" + (counter_order + 1) + ".</p>\n" +
        "                                </div>\n" +
        "                                <div class=\"col-md-6\">\n" +
        "                                    <p id=\"mitka_name\">Mitka-standard (" + colorElement + ")</p>\n" +
        "                                </div>\n" +
        "                                <div class=\"col-md-2\">\n" +
        "                                    <p id=\"number_of_mitka\">" + singleMitka.number + "шт</p>\n" +
        "                                </div>\n" +
        "                                <div class=\"col-md-2\">\n" +
        "                                    <p id=\"mitka_price\">$" + singleMitka.number * 10 + "</p>\n" +
        "                                </div>";
    element.setAttribute("class", "col-md-4 text-center my-auto");
    element.innerHTML = "<div class=\"row-padding text-center single-div\">\n" +
        "                        <div class=\"mitka-type-item text-center\">\n" +
        "                            <img src=\"../images/sinle_mitka.png\" alt=\"single_mitka\" id=\"single_mitka\">\n" +
        "                        </div>\n" +
        "                        <div class=\"row\" style=\"margin-top: 12px\">\n" +
        "                            <div class=\"col-md-12 text-center\">\n" +
        "                                <h4>Мітка-standard</h4>\n" +
        "                            </div>\n" +
        "                        </div>\n" +
        "                        <div class=\"row\" style=\"margin-top: 8px\">\n" +
        "                            <div class=\"col-md-6\">\n" +
        "                                <h6>Колір: </h6>\n" +
        "                            </div>\n" +
        "                            <div class=\"col-md-6\">\n" +
        "                                <h6 id=\"mitka_color\">" + colorElement + "</h6>\n" +
        "                            </div>\n" +
        "                        </div>\n" +
        "                        <div class=\"row\">\n" +
        "                            <div class=\"col-md-6\">\n" +
        "                                <h6>Кількість: </h6>\n" +
        "                            </div>\n" +
        "                            <div class=\"col-md-6\">\n" +
        "                                <h6 id=\"mitka_number\">" + number + "</h6>\n" +
        "                            </div>\n" +
        "                        </div>\n" +
        "                        <div class=\"row\">\n" +
        "                            <div class=\"col-md-6\">\n" +
        "                                <h6>Ціна: </h6>\n" +
        "                            </div>\n" +
        "                            <div class=\"col-md-6\">\n" +
        "                                <h6 id=\"mitka_price\">$" + 10 * number + "</h6>\n" +
        "                            </div>\n" +
        "                        </div>\n" +
        "<img src=\"../images/close.png\" onclick=\"delete_item(singlemitka/, id_arr)\" alt=\"close_image\"" + "id=" + i.toString() + " class=\"close-image\">\n" +
        "                    </div>";
    new_element.appendChild(element);
    new_order_element.appendChild(order_element);
    setOverallPrice();
    delete_item("singlemitka/", id_arr);
    i++;
    counter_order++;
}

function addSingleElementSet(setMitka) {
    let new_element = document.getElementById("single_mitka_item");
    let new_order_element = document.getElementById("mitka_order_item");
    let firstColor = setMitka.firstColor;
    let secondColor = setMitka.secondColor;
    let thirdColor = setMitka.thirdColor;
    let fourthColor = setMitka.fourthColor;
    overall_sum += 38;
    let firstColorCheck = checkColorSet(firstColor);
    let secondColorCheck = checkColorSet(secondColor);
    let thirdColorCheck = checkColorSet(thirdColor);
    let fourthColorCheck = checkColorSet(fourthColor);
    let element = document.createElement("div");
    let colorsCheck = [firstColorCheck, secondColorCheck, thirdColorCheck, fourthColorCheck];
    let id = setMitka._id;
    id_arr_set.push(id);
    mitkName.push("Сімейний набір");
    mitkaColor.push(colorsCheck);
    mitkaNumber.push(1);
    mitkaPrice.push("$38");
    let order_element = document.createElement("div");
    order_element.setAttribute("class", "row");
    order_element.innerHTML = "<div class=\"col-md-2\">\n" +
        "                                    <p id=\"number\">" + (counter_order + 1) + ".</p>\n" +
        "                                </div>\n" +
        "                                <div class=\"col-md-6\">\n" +
        "                                    <p id=\"mitka_name\">Сімейний набір (" + firstColorCheck + ", " + secondColorCheck + ", " + thirdColorCheck + ", " + fourthColorCheck + ")</p>\n" +
        "                                </div>\n" +
        "                                <div class=\"col-md-2\">\n" +
        "                                    <p id=\"number_of_mitka\">" + 1 + "шт</p>\n" +
        "                                </div>\n" +
        "                                <div class=\"col-md-2\">\n" +
        "                                    <p id=\"mitka_price\">$" + 38 + "</p>\n" +
        "                                </div>";
    element.setAttribute("class", "col-md-4 text-center my-auto");
    element.innerHTML = "<div class=\"row-padding text-center single-div\">\n" +
        "                        <div class=\"mitka-type-item text-center\">\n" +
        "                            <img src=\"../images/tracker.png\" alt=\"single_mitka\" id=\"tracker_mitka\">\n" +
        "                        </div>\n" +
        "                        <div class=\"row\" style=\"margin-top: 12px\">\n" +
        "                            <div class=\"col-md-12 text-center\">\n" +
        "                                <h4>Сімейний набір</h4>\n" +
        "                            </div>\n" +
        "                        </div>\n" +
        "                        <div class=\"row\" style=\"margin-top: 8px\">\n" +
        "                            <div class=\"col-md-6\">\n" +
        "                                <h6>Колір 1: </h6>\n" +
        "                            </div>\n" +
        "                            <div class=\"col-md-6\">\n" +
        "                                <h6 id=\"mitka_color1\">" + firstColorCheck + "</h6>\n" +
        "                            </div>\n" +
        "                        </div>\n" +
        "                        <div class=\"row\">\n" +
        "                            <div class=\"col-md-6\">\n" +
        "                                <h6>Колір 2: </h6>\n" +
        "                            </div>\n" +
        "                            <div class=\"col-md-6\">\n" +
        "                                <h6 id=\"mitka_color2\">" + secondColorCheck + "</h6>\n" +
        "                            </div>\n" +
        "                        </div>\n" +
        "                        <div class=\"row\">\n" +
        "                            <div class=\"col-md-6\">\n" +
        "                                <h6>Колір 3: </h6>\n" +
        "                            </div>\n" +
        "                            <div class=\"col-md-6\">\n" +
        "                                <h6 id=\"mitka_color3\">" + thirdColorCheck + "</h6>\n" +
        "                            </div>\n" +
        "                        </div>\n" +
        "                        <div class=\"row\">\n" +
        "                            <div class=\"col-md-6\">\n" +
        "                                <h6>Колір 4: </h6>\n" +
        "                            </div>\n" +
        "                            <div class=\"col-md-6\">\n" +
        "                                <h6 id=\"mitka_color4\">" + fourthColorCheck + "</h6>\n" +
        "                            </div>\n" +
        "                        </div>\n" +
        "                        <div class=\"row\">\n" +
        "                            <div class=\"col-md-6\">\n" +
        "                                <h6>Ціна: </h6>\n" +
        "                            </div>\n" +
        "                            <div class=\"col-md-6\">\n" +
        "                                <h6 id=\"mitka_price\">$38</h6>\n" +
        "                            </div>\n" +
        "                        </div>\n" +
        "<img src=\"../images/close.png\" onclick=\"delete_item(setmitka/, id_arr_set)\" alt=\"close_image\"" + "id=" + j.toString() + " class=\"close-image\">\n" +
        "                    </div>";
    new_element.appendChild(element);
    new_order_element.appendChild(order_element);
    setOverallPrice();
    delete_item("setmitka/", id_arr_set);
    j++;
    counter_order++;
}

function checkColorSet(color) {
    let colorElementSet;
    switch (color) {
        case "orange_white_set":
            return colorElementSet = "оранжево-білий";
        case "purple_white_set":
            return colorElementSet = "фіолетово-білий";
        case "white_gray_set":
            return colorElementSet = "біло-сірий";
        case "yellow_gray_set":
            return colorElementSet = "жовто-сірий";
        case "blue_yellow_set":
            return colorElementSet = "блакитно-жовтий";
        case "red_white_set":
            return colorElementSet = "червоно-білий";
    }
}

function setOverallPrice() {
    price.innerHTML = "$" + overall_sum;
    total_price.innerText = "Всього: $" + overall_sum;
}

function delete_item(key, arr) {
    let close_button = document.getElementsByClassName("close-image");
    for (let item of close_button) {
        item.onclick = function () {
            let item_number = item.id;
            let req = new XMLHttpRequest();
            req.responseType = 'json';
            req.open('DELETE', DOMAINDB + key + arr[item_number], true);
            req.onload = () => req.status === 200 ? callback(req.response) : console.log(req.response);
            req.send(null);
            window.location = window.location.pathname + "?message=Email+Successfully+Sent%21&isError=0";
        };
    }
}

btn.onclick = function () {
    if (mitkName.length === 0) {
        modal.style.display = "none";
        alert("У вас порожній кошик!");
    } else {
        modal.style.display = "block";
    }
};

span.onclick = function () {
    modal.style.display = "none";
};

window.onclick = function (event) {
    if (event.target === modal) {
        modal.style.display = "none";
    }
};


function sendOrder(key, data, del = true) {
    let req = new XMLHttpRequest();
    req.open("POST", DOMAIN + key, true);
    req.setRequestHeader('Content-type', 'application/json; charset=utf-8');
    req.onreadystatechange = console.log;
    req.send(JSON.stringify(data));
    if (!del) return;
}

function readInfo() {
    let name = document.getElementById("name-input").value;
    let phone = document.getElementById("phone-input").value;
    let email = document.getElementById("email-input").value;
    let city = document.getElementById("city-input").value;
    let office = document.getElementById("office-input").value;
    if (validateInfo(name, phone, email, city, office)) {
        return new UserOrder(name, phone, email, city, office, mitkName, mitkaColor, mitkaNumber, mitkaPrice);
    } else {
        console.log("oops");
        return false;
    }
}

function validateInfo(name, phone, email, city, office) {
    if (name.trim() !== "" && phone.trim() !== "" && city.trim() !== "" && office.trim() !== "" && ValidateEmail(email)) {
        return true;
    } else {
        alert("Заповніть всі поля коректно!");
        return false;
    }
}

function sendData() {
    if (readInfo() !== false) {
        sendOrder("set_order", readInfo());
        delete_all("singlemitkaAll");
        delete_all("setmitkaAll");
        window.location = window.location.pathname + "?message=Email+Successfully+Sent%21&isError=0";
    }
}

function ValidateEmail(mail) {
    if (/^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/.test(mail)) {
        return (true)
    }
    alert("Ви ввели неправильну адресу!");
    return (false);
}

function delete_all(key) {
    let req = new XMLHttpRequest();
    req.responseType = 'json';
    req.open('DELETE', DOMAINDB + key, true);
    req.onload = () => req.status === 200 ? callback(req.response) : console.log(req.response);
    req.send(null);
}

class UserOrder {
    constructor(name, phone, email, city, office, mitkaName, mitkaColor, mitkaNumber, mitkaPrice) {
        this.name = name || '';
        this.phone = phone || '';
        this.email = email || '';
        this.city = city || '';
        this.office = office || '';
        this.mitkaName = mitkaName || '';
        this.mitkaColor = mitkaColor || '';
        this.mitkaNumber = mitkaNumber || '';
        this.mitkaPrice = mitkaPrice || '';
    }
}