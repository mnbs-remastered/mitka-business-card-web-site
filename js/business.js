var modal = document.getElementById("myModal");

var btn = document.getElementById("order_button");

var span = document.getElementsByClassName("close")[0];

btn.onclick = function() {
    modal.style.display = "block";
};

span.onclick = function() {
    modal.style.display = "none";
};

window.onclick = function(event) {
    if (event.target == modal) {
        modal.style.display = "none";
    }
};

var data_js = {
    "access_token": "1xhoesoekhii5xoub5lp9bnz"
};

function js_onSuccess() {
    window.location = window.location.pathname + "?message=Email+Successfully+Sent%21&isError=0";
}

function js_onError(error) {
    window.location = window.location.pathname + "?message=Email+could+not+be+sent.&isError=1";
}

var sendButton = document.getElementById("send_button");

function js_send() {
    sendButton.value='Sending…';
    sendButton.disabled=true;
    var request = new XMLHttpRequest();
    request.onreadystatechange = function() {
        if (request.readyState == 4 && request.status == 200) {
            js_onSuccess();
        } else
        if(request.readyState == 4) {
            js_onError(request.response);
        }
    };

    var name = document.getElementById("name-input").value;
    var surname = document.getElementById("surname-input").value;
    var email = document.getElementById("email-input").value;

    if (name.trim() != "") {
        if (surname.trim() != "") {
            if (ValidateEmail(email)) {
                var subject = "Купівля MitkaCEO продукту";
                var message = document.getElementById("name-input").value + " " + document.getElementById("surname-input").value + " " + "хоче купити MitkaCEO. Його email: "
                    + document.getElementById("email-input").value;
                data_js['subject'] = subject;
                data_js['text'] = message;
                var params = toParams(data_js);

                sendButton.style.background = "#757575";
                request.open("POST", "https://postmail.invotes.com/send", true);
                request.setRequestHeader("Content-type", "application/x-www-form-urlencoded");

                request.send(params);
                return false;
            } else {
                return;
            }
        } else {
            alert("Введіть прізвище!");
        }
    } else {
        alert("Введіть ім'я!");
    }

}

sendButton.onclick = js_send;

function toParams(data_js) {
    var form_data = [];
    for ( var key in data_js ) {
        form_data.push(encodeURIComponent(key) + "=" + encodeURIComponent(data_js[key]));
    }

    return form_data.join("&");
}

/**
 * @return {boolean}
 */
function ValidateEmail(mail)
{
    if (/^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/.test(mail))
    {
        return (true)
    }
    alert("Ви ввели неправильну адресу!");
    return (false);
}