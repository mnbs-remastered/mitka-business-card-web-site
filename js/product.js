let modal = document.getElementById("myModal");

let btn = document.getElementById("individ_order");

let btnSet = document.getElementById("individ_order_set");

let span = document.getElementsByClassName("close")[0];

let sendButton = document.getElementById("send_button");

let mitkaColor = undefined;

let mitkaColorSet = new Set();

let tickImage = document.querySelectorAll(".tick-image");

let tickImageSet = document.querySelectorAll(".tick-image-set");


function tickTurnOn(id) {
    switch (id) {
        case "purple_white":
            tickSetting(0);
            mitkaColor = "purple_white";
            break;
        case "white_gray":
            tickSetting(1);
            mitkaColor = "white_gray";
            break;
        case "yellow_gray":
            tickSetting(2);
            mitkaColor = "yellow_gray";
            break;
        case "blue_yellow":
            tickSetting(3);
            mitkaColor = "blue_yellow";
            break;
        case "red_white":
            tickSetting(4);
            mitkaColor = "red_white";
            break;
        case "orange_white":
            tickSetting(5);
            mitkaColor = "orange_white";
            break;
    }
}

function tickTurnOnSet(id) {
    switch (id) {
        case "purple_white_set":
            pushToTheColorSet("purple_white_set");
            break;
        case "white_gray_set":
            pushToTheColorSet("white_gray_set");
            break;
        case "yellow_gray_set":
            pushToTheColorSet("yellow_gray_set");
            break;
        case "blue_yellow_set":
            pushToTheColorSet("blue_yellow_set");
            break;
        case "red_white_set":
            pushToTheColorSet("red_white_set");
            break;
        case "orange_white_set":
            pushToTheColorSet("orange_white_set");
            break;
    }
}

function pushToTheColorSet(color) {
    if (mitkaColorSet.size === 4) {
        let firstValue = mitkaColorSet.values().next().value;
        mitkaColorSet.delete(firstValue);
        switch (firstValue) {
            case "purple_white_set":
                tickImageSet[0].style.display = "none";
                break;
            case "white_gray_set":
                tickImageSet[1].style.display = "none";
                break;
            case "yellow_gray_set":
                tickImageSet[2].style.display = "none";
                break;
            case "blue_yellow_set":
                tickImageSet[3].style.display = "none";
                break;
            case "red_white_set":
                tickImageSet[4].style.display = "none";
                break;
            case "orange_white_set":
                tickImageSet[5].style.display = "none";
                break;
        }
    }
    mitkaColorSet.add(color);

    switch (color) {
        case "purple_white_set":
            tickImageSet[0].style.display = "block";
            break;
        case "white_gray_set":
            tickImageSet[1].style.display = "block";
            break;
        case "yellow_gray_set":
            tickImageSet[2].style.display = "block";
            break;
        case "blue_yellow_set":
            tickImageSet[3].style.display = "block";
            break;
        case "red_white_set":
            tickImageSet[4].style.display = "block";
            break;
        case "orange_white_set":
            tickImageSet[5].style.display = "block";
            break;
    }
}

function tickSetting(allowed) {
    for (i = 0; i < tickImage.length; i++) {
        tickImage[i].style.display = "none";
    }
    tickImage[allowed].style.display = "block";
}


btn.onclick = function () {
    modal.style.display = "block";
};

btnSet.onclick = function () {
    modal.style.display = "block";
};

span.onclick = function () {
    modal.style.display = "none";
};

window.onclick = function (event) {
    if (event.target === modal) {
        modal.style.display = "none";
    }
};

const DOMAIN = 'http://localhost:3000/';
const DOMAINDB = 'http://localhost:3030/';

function sendToServer(key, data, del = true) {
    if (data.color) {
        let req = new XMLHttpRequest();
        req.open("POST", DOMAIN + key, true);
        req.setRequestHeader('Content-type', 'application/json; charset=utf-8');
        req.onreadystatechange = console.log;
        req.send(JSON.stringify(data));
    }
    if (!del) return;
}

function readInfo() {
    let name = document.getElementById("name-input").value;
    let surname = document.getElementById("surname-input").value;
    let text = document.getElementById("text-input").value;
    let color = document.getElementById("color-input").value;
    let shape = document.getElementById("shape-input").value;
    let quantity = document.getElementById("quantity-input").value;
    let email = document.getElementById("email-input").value;
    if (validateInfo(name, surname, text, color, shape, quantity, email)) {
        let order = new IndividOrder(name, surname, text, color, shape, quantity, email);
        return order;
    } else {
        console.log("oops");
        return false;
    }
}

function validateInfo(name, surname, text, color, shape, quantity, email) {
    if (name.trim() !== "" && surname.trim() !== "" && text.trim() !== "" && color.trim() !== "" && shape.trim() !== "" && quantity.trim() !== "" && ValidateEmail(email)) {
        return true;
    } else {
        alert("Заповніть всі поля коректно!");
        return false;
    }
}

function sendData() {
    if (readInfo() !== false) {
        sendToServer("individ_order", readInfo());
        sendButton.style.background = "#757575";
        window.location = window.location.pathname + "?message=Email+Successfully+Sent%21&isError=0";
        console.log("sent!");
    }
}

function ValidateEmail(mail) {
    if (/^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/.test(mail)) {
        return (true)
    }
    alert("Ви ввели неправильну адресу!");
    return (false);
}

class IndividOrder {
    constructor(name, surname, text, color, shape, quantity, email) {
        this.name = name || '';
        this.surname = surname || '';
        this.text = text || '';
        this.color = color || '';
        this.shape = shape || '';
        this.quantity = quantity || '';
        this.email = email || '';
    }
}

function increaseValue() {
    let currentValue = document.getElementById("number_mitka").textContent;
    let currentNumber = Number(currentValue);
    if (currentNumber > 9) {
        alert("Забагато міток!");
        return;
    }
    let newNumber = currentNumber + 1;
    let newValue = newNumber.toString();
    let numberText = document.getElementById("number_mitka").innerHTML = newValue;
    setPrice();
}

function decreaseValue() {
    let currentValue = document.getElementById("number_mitka").textContent;
    let currentNumber = Number(currentValue);
    if (currentNumber === 1) {
        alert("Замало міток!");
        return;
    }
    let newNumber = currentNumber - 1;
    let newValue = newNumber.toString();
    let numberText = document.getElementById("number_mitka").innerHTML = newValue;
    setPrice();
}

function setPrice() {
    let currentValue = document.getElementById("number_mitka").textContent;
    let currentNumberPrice = 10 * Number(currentValue);
    let currentTextPrice = currentNumberPrice.toString();
    let newTextPrice = document.getElementById("price_mitka").innerHTML = "$" + currentTextPrice;
}

class SingleMitka {
    constructor(numberOfMitka, color) {
        this.numberOfMitka = numberOfMitka || '';
        this.color = color || '';
    }
}

class SetMitka {
    constructor(firstColor, secondColor, thirdColor, fourthColor) {
        this.firstColor = firstColor || '';
        this.secondColor = secondColor || '';
        this.thirdColor = thirdColor || '';
        this.fourthColor = fourthColor || '';
    }
}

function sendDataToDB(key = "singlemitka") {
    if (mitkaColor !== undefined) {
        let numberOfMitka = document.getElementById("number_mitka").textContent;
        let singleMitka = new SingleMitka(numberOfMitka, mitkaColor);

        let req = new XMLHttpRequest();
        req.open("POST", DOMAINDB + key, true);
        req.setRequestHeader('Content-type', 'application/json; charset=utf-8');
        req.onreadystatechange = console.log;
        req.send(JSON.stringify(singleMitka));
        window.location = window.location.pathname + "?message=Email+Successfully+Sent%21&isError=0";
        console.log("sent!");
    } else {
        alert("Оберіть колір!");
    }
}

function sendDataToDBSet(key = "setmitka") {
    if (mitkaColorSet.size === 4) {
        let mitkaColorIter = mitkaColorSet.values();
        let setMitka = new SetMitka(mitkaColorIter.next().value, mitkaColorIter.next().value,
            mitkaColorIter.next().value, mitkaColorIter.next().value);

        let req = new XMLHttpRequest();
        req.open("POST", DOMAINDB + key, true);
        req.setRequestHeader('Content-type', 'application/json; charset=utf-8');
        req.onreadystatechange = console.log;
        req.send(JSON.stringify(setMitka));
        window.location = window.location.pathname + "?message=Email+Successfully+Sent%21&isError=0";
        console.log("sent!");
    } else {
        alert("Оберіть колір!");
    }
}